<a href="<?php the_permalink() ?>" class="news_item">
	<div class="photo">
		<img src="<?php
		$thumb_id = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_src($thumb_id, "news-360x240", true);
		echo $thumb_url[0];
		?>" alt="<?php the_title(); ?>" />
	</div>
	<p class="title"><?php the_title(); ?></p>
	<div class="news_text">
		<p><?php the_field('short_description');
		?></p>
	</div>
</a>