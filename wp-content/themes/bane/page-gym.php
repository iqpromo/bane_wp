<?php
/*
Template Name: Gym
*/
?>

<?php get_header(); ?>

<div class="about_gym_section section before_section_bg texture_bg">
	<div class="container">
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p class="header_3">Наш зал</p>
				<div class="wrapper">
					<a href="<?php bloginfo('template_url'); ?>/img/bgs/gym_bg.jpg" data-fancybox="group" class="photo_item fancybox">
						<img src="<?php bloginfo('template_url'); ?>/img/gym_photo-1.jpg" alt="Bane Crossfit" />
					</a>

					<a href="<?php bloginfo('template_url'); ?>/img/bgs/crossfit_bg.png" data-fancybox="group" class="photo_item fancybox">
						<img src="<?php bloginfo('template_url'); ?>/img/gym_photo-2.jpg" alt="Bane Crossfit" />
					</a>

					<a href="<?php bloginfo('template_url'); ?>/img/bgs/news_bg.jpg" data-fancybox="group" class="photo_item fancybox">
						<img src="<?php bloginfo('template_url'); ?>/img/gym_photo-3.jpg" alt="Bane Crossfit" />
					</a>

					<a href="<?php bloginfo('template_url'); ?>/img/bgs/gym_bg.jpg" data-fancybox="group" class="photo_item fancybox">
						<img src="<?php bloginfo('template_url'); ?>/img/gym_photo-1.jpg" alt="Bane Crossfit" />
					</a>

					<a href="<?php bloginfo('template_url'); ?>/img/bgs/gym_bg.jpg" data-fancybox="group" class="photo_item fancybox">
						<img src="<?php bloginfo('template_url'); ?>/img/gym_photo-1.jpg" alt="Bane Crossfit" />
					</a>

					<a href="<?php bloginfo('template_url'); ?>/img/bgs/crossfit_bg.png" data-fancybox="group" class="photo_item fancybox">
						<img src="<?php bloginfo('template_url'); ?>/img/gym_photo-2.jpg" alt="Bane Crossfit" />
					</a>

					<a href="<?php bloginfo('template_url'); ?>/img/bgs/news_bg.jpg" data-fancybox="group" class="photo_item fancybox">
						<img src="<?php bloginfo('template_url'); ?>/img/gym_photo-3.jpg" alt="Bane Crossfit" />
					</a>

					<a href="<?php bloginfo('template_url'); ?>/img/bgs/gym_bg.jpg" data-fancybox="group" class="photo_item fancybox">
						<img src="<?php bloginfo('template_url'); ?>/img/gym_photo-1.jpg" alt="Bane Crossfit" />
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>