<?php

add_theme_support( 'post-thumbnails' );
add_image_size( 'news-360x240', 360, 240, true );
add_image_size( 'single_news-625x395', 625, 395, true );
/**
 * bane functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package bane
 */

if ( ! function_exists( 'bane_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function bane_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on bane, use a find and replace
		 * to change 'bane' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'bane', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'bane_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'bane_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function bane_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'bane_content_width', 640 );
}
add_action( 'after_setup_theme', 'bane_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function bane_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'bane' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'bane' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'bane_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function bane_scripts() {
	wp_enqueue_style( 'bane-style', get_stylesheet_uri() );

	wp_enqueue_script( 'bane-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'bane-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'bane_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}






function bane_styles(){

	wp_register_style( 'bootstrap', get_template_directory_uri() . '/bootstrap-4/css/bootstrap.css', array(), 'screen');
	wp_register_style( 'fancybox', get_template_directory_uri() . '/css/jquery.fancybox.css', array(), 'screen');
	wp_register_style( 'common_styles', get_template_directory_uri() . '/css/common.css', array(), 'screen');
	wp_register_style( 'index', get_template_directory_uri() . '/css/index.css', array(), 'screen');
	wp_register_style( 'media_q', get_template_directory_uri() . '/css/media-q.css', array(), 'screen');
	wp_register_style( 'font_awesome', get_template_directory_uri() . '/font-awesome/css/font-awesome.css', array(), 'screen');

    // Подключаю стили
	wp_enqueue_style( 'bootstrap');
	wp_enqueue_style( 'fancybox');
	wp_enqueue_style( 'common_styles');
	wp_enqueue_style( 'index');
	wp_enqueue_style( 'media_q');
	wp_enqueue_style( 'font_awesome');
}




function my_bane_scripts(){

	wp_enqueue_script( 'jquery_js', get_template_directory_uri() . '/js/jquery-3.2.0.min.js', array(), '', true );
	wp_enqueue_script( 'bootstrap_popper', get_template_directory_uri() . '/bootstrap-4/js/popper.min.js', array(), '', true );
	wp_enqueue_script( 'bootstrap_js', get_template_directory_uri() . '/bootstrap-4/js/bootstrap.js', array(), '', true );
	wp_enqueue_script( 'fancybox_js', get_template_directory_uri() . '/js/jquery.fancybox.js', array(), '', true );
	wp_enqueue_script( 'maskedinput', get_template_directory_uri() . '/js/jquery.maskedinput.min.js', array(), '', true );
	wp_enqueue_script( 'index_js', get_template_directory_uri() . '/js/index.js', array(), '', true );
}


add_action( 'wp_enqueue_scripts', 'bane_styles', 1 );
add_action( 'wp_enqueue_scripts', 'my_bane_scripts', 1 );



register_nav_menus( array(
	'header_menu' 					=> esc_html__( 'Главное меню' ),
	'footer_menu' 					=> esc_html__( 'Нижнее меню' )
) );


// Register Custom Navigation Walker
require_once get_template_directory() . '/wp-bootstrap-navwalker.php';



function pagenavi_paged($q) {
	$paged = get_query_var('paged') ? get_query_var('paged') : 1;
	$q->set('paged', $paged);
}
add_action('pre_get_posts', 'pagenavi_paged');

// Replace wpv-pagination shortcode
function wpv_pagenavi($args, $content) {
	global $WP_Views;
	return wp_pagenavi( array('query' => $WP_Views->post_query, 'echo'=>false) );
}
add_shortcode('wpv-pagination', 'wpv_pagenavi');