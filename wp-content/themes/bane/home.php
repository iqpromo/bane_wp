<?php
/*
Template Name: Home
*/
?>

<?php get_header(); ?>


<div class="advantages_section section before_section_bg texture_bg">
	<div class="container">
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p class="header_3">Преимущества</p>
				<div class="wrapper">
					<div class="advantage_item">
						<div class="photo">
							<img src="<?php bloginfo('template_url'); ?>/img/icons/m2_icon.png" width="48" height="34" alt="Просторный зал" >
						</div>
						<p class="title">S = 500 кв.м.</p>
						<p>Просторный зал, где<br>никому не тесно</p>
					</div>

					<div class="advantage_item">
						<div class="photo">
							<img src="<?php bloginfo('template_url'); ?>/img/icons/crossfit_icon.png" width="84" height="24" alt="Аффилиат" >
						</div>
						<p class="title">Аффилиат</p>
						<p>Зал и тренера прошли<br>сертификацию Crossfit Inc</p>
					</div>

					<div class="advantage_item">
						<div class="photo">
							<img src="<?php bloginfo('template_url'); ?>/img/icons/fitness_icon.png" width="79" height="60" alt="Тренерский состав" >
						</div>
						<p class="title">Тренерский состав</p>
						<p>Профессиональные тренера со<br>стажем преподавания более 5 лет</p>
					</div>

					<div class="advantage_item">
						<div class="photo">
							<img src="<?php bloginfo('template_url'); ?>/img/icons/compas_icon.png" width="39" height="72" alt="Зал на юге" >
						</div>
						<p class="title">Зал на юге</p>
						<p>Первый кроссфит зал<br>на юге СПБ</p>
					</div>

					<div class="advantage_item">
						<div class="photo">
							<img src="<?php bloginfo('template_url'); ?>/img/icons/dumbbell_icon.png" width="78" height="78" alt="Оснащение" >
						</div>
						<p class="title">Оснащение</p>
						<p>Качественное оборудование<br>для любых видов тренировок</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="gym_section section">
	<div class="overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12">
				<img src="<?php bloginfo('template_url'); ?>/img/gym_photo.png" width="501" height="308" alt="Наш зал" />
			</div>
			<div class="col-xl-6 col-lg-5 col-md-12 col-sm-12 col-xs-12">
				<div class="section_text">
					<p class="header_3">Наш зал</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse fringilla fringilla nisl congue congue. Maecenas nec condimentum libero, at elementum mauris. Phasellus eget nisi dapibus, ultricies nisl at, hendrerit risusuis ornare luctus id sollicitudin ante lobortis at.</p>
					<p>Maecenas nec condimentum libero, at elementum mauris. Phasellus eget nisi dapibus, ultricies nisl at, hendrerit risusuis ornare luctus id sollicitudin ante lobortis at.</p>
					<a href="/gym" class="brown_btn">Фото зала</a>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="abonements_section section before_section_bg texture_bg">
	<div class="container">
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p class="header_3">Выбери абонемент</p>
				<p class="subheader">на тренировки</p>
				<div class="wrapper">
					<div class="abonement_item">
						<p class="header_4">Вводный курс</p>
						<p class="header_5">включает в себя <br>12 обучающих занятий</p>
						<p class="price">2500</p>
						<a href="/price" class="brown_btn">Записаться</a>
					</div>

					<div class="abonement_item">
						<p class="header_4">1 месяц CrossFit</p>
						<p class="header_5">безлимитное <br>посещение</p>
						<p class="price">5500</p>
						<a href="/price" class="brown_btn">Купить</a>
					</div>

					<div class="abonement_item">
						<p class="header_4">Акционные пакеты</p>
						<p class="header_5">экономия при покупке <br>долгосрочных абонементов</p>
						<p class="price">от 2500</p>
						<a href="/price" class="brown_btn">Подробнее</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="crossfit_section section">
	<div class="overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12">
				<div class="section_text">
					<p class="header_3">Что такое CrossFit?</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse fringilla fringilla nisl congue congue. Maecenas nec condimentum libero, at elementum mauris. Phasellus eget nisi dapibus, ultricies nisl at, hendrerit risusuis ornare luctus id sollicitudin ante lobortis at.</p>

					<div class="subscribe_form">
						<?php echo do_shortcode( '[contact-form-7 id="7" title="Подписаться"]' ); ?>
					</div>
				</div>
			</div>
			<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12">

				<div class="video">
					<div class="video_wrapper">
						<iframe src="https://www.youtube.com/embed/1aVw1gZ9Ncg?rel=0" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>


<div class="form_section section red_before_bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p class="header_3">Запишись на пробную тренировку</p>
				<form class="trial_fit_form">
					<input type="text" placeholder="Имя" name="name" />
					<input type="tel" placeholder="Телефон" id="phone" name="phone" class="phone_form" />
					<label>или</label>
					<input type="email" placeholder="E-mail" name="email" />
					<button type="submit" class="white_btn">Записаться</button>
				</form>
			</div>
		</div>
	</div>
</div>


<div class="news_section section">
	<div class="overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p class="header_3">Новости</p>
				<div class="wrapper">

					<?php
					$args = array(
						'post_type' => 'news',
						'posts_per_page' => '3',
						'orderby'     => 'date'
					);
					$services_query = new WP_Query( $args );
					while ($services_query->have_posts()) : $services_query->the_post(); ?>

					<?php include "template-parts/news.php"; ?>

				<?php endwhile;
				wp_reset_query();
				wp_reset_postdata(); ?>

			</div>

			<a href="/news_blog" class="all_news">Все новости</a>
		</div>
	</div>
</div>
</div>


<div class="contacts_section section before_section_bg texture_bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p class="header_3">Наши контакты</p>
				<p class="address">г. Санкт-Петербург, ул. Марата, д. 23</p>
				<a href="tel:+7-812-812-12-12" class="phone bold">+7 (812) 812 -1212</a>
			</div>
		</div>
	</div>

	<div id="map" class="map">
		<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
		<script type="text/javascript">
			/* Yandex Api Map Script */
			ymaps.ready(function () {
				var myMap = new ymaps.Map('map', {
					center: [59.92789206420742,30.352860499999945],
					zoom: 17
				}, {
					searchControlProvider: 'yandex#search'
				}),

				myPlacemark = new ymaps.Placemark([59.92789206420742,30.352860499999945], {
					balloonContent: '<p class="baloon">Санкт-Петербург, ул. Марата, д. 23</p>'
				}, {

               // iconLayout: 'default#image',
               // iconImageHref: '<?php bloginfo('template_url'); ?>/img/icons/map_baloon.png',
               preset: 'islands#redSportIcon',
               iconColor: '#b31c27',

               balloonCloseButton: true,
               balloonPanelMaxMapArea: 'Infinity'
             });

				myMap.behaviors.disable('scrollZoom'),

				myMap.geoObjects
				.add(myPlacemark);
			});
			/* End of Yandex Api Map Script */
		</script>
	</div>
</div>


<?php get_footer(); ?>