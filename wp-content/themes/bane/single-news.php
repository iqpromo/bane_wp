<?php get_header(); ?>

<div class="single_news_section section before_section_bg texture_bg">
	<div class="container">
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p class="header_3">Новости</p>

				<div class="single_news clearfix">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<div class="single_news_photo right">
							<img src="<?php
							$thumb_id = get_post_thumbnail_id();
							$thumb_url = wp_get_attachment_image_src($thumb_id, "single_news-625x395", true);
							echo $thumb_url[0];
							?>" alt="<?php the_title(); ?>" />
						</div>
						<p class="title"><?php the_title(); ?></p>
						<?php the_content(); ?>
					<?php endwhile; else: ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
</div>

<?php get_footer(); ?>