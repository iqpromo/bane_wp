<?php
/*
Template Name: Price
*/
?>

<?php get_header(); ?>

<div class="price_section section before_section_bg texture_bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p class="header_3">Цены</p>
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item small">
						<a class="nav-link active" data-toggle="tab" href="#introductory" role="tab">
						Вводный курс</a>
					</li>
					<li class="nav-item middle">
						<a class="nav-link" data-toggle="tab" href="#groups" role="tab">Групповые тренировки</a>
					</li>
					<li class="nav-item big">
						<a class="nav-link" data-toggle="tab" href="#one_visit" role="tab">разовые посещения <br>и самостоятельные тренировки</a>
					</li>
					<li class="nav-item small">
						<a class="nav-link" data-toggle="tab" href="#extra_service" role="tab">Доп. услуги</a>
					</li>
					<li class="nav-item middle">
						<a class="nav-link" data-toggle="tab" href="#children_group" role="tab">Тренировки для детей</a>
					</li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<div class="tab-pane active" id="introductory" role="tabpanel">
						<p class="header_5 description">Курс рассчитан на новичков в направлении CrossFit. На курсе рассматриваются все базовые упражнения и техника их выполнения, а нагрузка растёт плавно от первой тренировки. Обязателен для тех, кто не занимался до это CrossFit.</p>
						<div class="wrapper">
							<div class="train_item introductory_block">
								<p class="name">VWODный курс</p>
								<p class="price">2500 р</p>
								<p class="trains_amount">12 тренировок за 4 недели</p>
								<button class="white_btn">Записаться</button>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="groups" role="tabpanel">
						<p class="header_3">Безлимитные</p>
						<p class="header_5">(посещение любых групповых тренировок в любое время)</p>
						<div class="wrapper">
							<div class="train_item">
								<p class="name">1 месяц</p>
								<p class="price">5500 р</p>
								<button class="white_btn">Купить</button>
							</div>

							<div class="train_item">
								<p class="name">3 месяца</p>
								<p class="price">14900 р</p>
								<p>(скидка 1750)</p>
								<button class="white_btn">Купить</button>
							</div>

							<div class="train_item">
								<p class="name">6 месяцев</p>
								<p class="price">27500 р</p>
								<p>(скидка 5500)</p>
								<button class="white_btn">Купить</button>
							</div>

							<div class="train_item">
								<p class="name">12 месяцев</p>
								<p class="price">49000 р</p>
								<p>(скидка 17000)</p>
								<button class="white_btn">Купить</button>
							</div>
						</div>

						<p class="header_3">Утренние безлимитные</p>
						<p class="header_5">(посещение любых групповых тренировок с 8.00 до 16.00)</p>
						<div class="wrapper">
							<div class="train_item">
								<p class="name">1 месяц</p>
								<p class="price">4500 р</p>
								<button class="white_btn">Купить</button>
							</div>

							<div class="train_item">
								<p class="name">3 месяца</p>
								<p class="price">11900 р</p>
								<p>(скидка 1600)</p>
								<button class="white_btn">Купить</button>
							</div>

							<div class="train_item">
								<p class="name">6 месяцев</p>
								<p class="price">22000 р</p>
								<p>(скидка 5000)</p>
								<button class="white_btn">Купить</button>
							</div>

							<div class="train_item">
								<p class="name">12 месяцев</p>
								<p class="price">40000 р</p>
								<p>(скидка 14000)</p>
								<button class="white_btn">Купить</button>
							</div>
						</div>

						<div class="wrapper">
							<div class="short_block">
								<p class="header_3">По занятиям</p>
								<p class="header_5">(посещение любых групповых тренировок <br>указанное кол-во раз за 1 месяц)</p>
								<div class="wrapper">
									<div class="train_item">
										<p class="name">8 тренировок</p>
										<p class="price">4500 р</p>
										<button class="white_btn">Купить</button>
									</div>

									<div class="train_item">
										<p class="name">12 тренировок</p>
										<p class="price">5000 р</p>
										<button class="white_btn">Купить</button>
									</div>
								</div>
							</div>

							<div class="short_block">
								<p class="header_3">Разовые</p>
								<p class="header_5">2 тренировки <br>в течении недели</p>
								<div class="wrapper">
									<div class="train_item">
										<p class="name">Пробное <br>посещение</p>
										<p class="price up">бесплатно</p>
										<button class="white_btn">Записаться</button>
									</div>

									<div class="train_item">
										<p class="name">Разовое <br>посещение</p>
										<p class="price">600 р</p>
										<button class="white_btn">Купить</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="one_visit" role="tabpanel">
						<p class="header_3">2 тренировки в течении недели</p>
						<p class="header_5">Самостоятельные тренировки в любое время. Приоритет оборудования и пространства принадлежит группе.</p>
						<div class="wrapper">
							<div class="train_item">
								<p class="name">Пробное <br>посещение</p>
								<p class="price up">бесплатно</p>
								<button class="white_btn">Записаться</button>
							</div>

							<div class="train_item">
								<p class="name">Разовое <br>посещение</p>
								<p class="price">600 р</p>
								<button class="white_btn">Купить</button>
							</div>

							<div class="train_item">
								<p class="name">OpenGym <br>1 месяц</p>
								<p class="price">2700 р</p>
								<button class="white_btn">Купить</button>
							</div>

							<div class="train_item">
								<p class="name">OpenGym <br>1 месяц</p>
								<p class="price">7000 р</p>
								<p>(скидка 1100)</p>
								<button class="white_btn">Купить</button>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="extra_service" role="tabpanel">
						<p class="header_5">С полным перечнем дополнительных услуг вы можете ознакомится на ресепшн клуба.</p>
						<p class="header_5 description">Для использования услуг необходимо приобрести абонемент OpenGym или любой групповой.</p>
						<div class="wrapper">
							<div class="train_item">
								<p class="name">Персональная <br>тренировка</p>
								<p class="price">(1 чел) 1500 р</p>
								<button class="white_btn">Записаться</button>
							</div>

							<div class="train_item">
								<p class="name">Персональная <br>тренировка</p>
								<p class="price">(2 чел) 2500 р</p>
								<button class="white_btn">Записаться</button>
							</div>

							<div class="train_item">
								<p class="name">Массаж <br>с элементами <br>остеопатии</p>
								<p class="price">(1 час) 1500 р</p>
								<button class="white_btn">Записаться</button>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="children_group" role="tabpanel">
						<p class="header_5">Мы запустим тренировки для детей совсем скоро! Следите за обновлениями.</p>
					</div>
				</div>

				<div class="wrapper">
					<div class="discount_item">
						<p class="amount">10%</p>
						<p class="description">Cкидка при <br>переходе из <br>другого клуба</p>
					</div>

					<div class="discount_item">
						<p class="amount">10%</p>
						<p class="description">Cкидка на 1ый <br>месяц после <br>вводного курса</p>
					</div>

					<div class="discount_item">
						<p class="amount">10%</p>
						<p class="description">Cкидка <br>для студентов</p>
					</div>

					<div class="discount_item">
						<p class="amount">10%</p>
						<p class="description">Cкидка для <br>кмс, <br>мс, ммс</p>
					</div>

					<div class="discount_item">
						<p class="amount">50%</p>
						<p class="description">Cкидка для <br>тренеров <br>других клубов</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>