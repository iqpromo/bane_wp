<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="UTF-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="viewport" content="initial-scale=1.0, width=device-width" />
	<meta property="og:image" content="<?php bloginfo('template_url'); ?>/img/miniature.jpg" />

	<?php wp_head() ?>


	<link rel="icon" type="image/png" href="/favicon.ico"  />
	<link rel="shortcut icon" type="image/png" href="/favicon.ico"  />
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic-ext,latin-ext" rel="stylesheet" />



	<title><?php wp_title();?></title>
    <!--[if IE]>
      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
      <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
  </head>

  <body <?php body_class(); ?>>
  	<header>
  		<div class="top_header">
  			<div class="container">
  				<div class="row">
  					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">

  						<nav class="navbar navbar-expand-lg">
  							<button type="button" data-target="#main_navigation" data-toggle="collapse" class="navbar-toggler">
  								<span class="icon-bar"></span>
  								<span class="icon-bar"></span>
  								<span class="icon-bar"></span>
  							</button>

  							<?php
  							wp_nav_menu( array(
  								'menu'              => 'header_menu',
  								'depth'             => 1,
  								'container'         => 'div',
  								'container_id'      => 'main_navigation',
  								'container_class'   => 'navbar-collapse collapse',
  								'menu_class'        => 'main_navigation nav navbar-nav',
  								'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
  								'walker'          => new WP_Bootstrap_Navwalker())
  						);
  						?>

  						<a href="tel:+7-812-812-12-12" class="callback"><i class="fa fa-phone" aria-hidden="true"></i><span>(812) 812-12-12</span></a>
  						<div class="socials">
  							<a href="#" class="vk"><i class="fa fa-vk" aria-hidden="true"></i></a>
  							<a href="#" class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a>
  							<a href="#" class="inst"><i class="fa fa-instagram" aria-hidden="true"></i></a>
  						</div>
  					</nav>

  				</div>
  			</div>
  		</div>
  	</div>

  	<div id="collection_carousel" class="carousel slide" data-ride="carousel">
  		<ol class="carousel-indicators">
  			<li data-target="#collection_carousel" data-slide-to="0" class="active"></li>
  			<li data-target="#collection_carousel" data-slide-to="1"></li>
  			<li data-target="#collection_carousel" data-slide-to="2"></li>
  		</ol>

  		<div class="carousel-inner">

  			<div class="carousel-item active">
  				<img src="<?php bloginfo('template_url'); ?>/img/header_slide-1.jpg" alt="BANE CrossFit" />

  				<div class="carousel-caption">
  					<div class="caption_inner">
  						<p class="header_1">B.A.N.E. CrossFit</p>
  						<p class="header_2">Скидка на вводный курс!</p>
  						<p class="header_5">Мы всегда рады новичкам в нашем зале.</p>
  						<p class="header_5">Вливайся в кроссфит-движение, <br>теперь всего лишь за 2000 рублей (8 тренировок).</p>
  						<p class="header_6">При покупке абонемента до 15 сентября.</p>
  						<button class="brown_btn" data-toggle="modal" data-target="#train_modal">Записаться на тренировку</button>
  					</div>
  				</div>
  			</div>

  			<div class="carousel-item">
  				<img src="<?php bloginfo('template_url'); ?>/img/header_slide-1.jpg" alt="BANE CrossFit" />

  				<div class="carousel-caption">
  					<div class="caption_inner">
  						<p class="header_1">B.A.N.E. CrossFit</p>
  						<p class="header_2">Скидка на вводный курс!</p>
  						<p class="header_5">Мы всегда рады новичкам в нашем зале.</p>
  						<p class="header_5">Вливайся в кроссфит-движение, <br>теперь всего лишь за 2000 рублей (8 тренировок).</p>
  						<p class="header_6">При покупке абонемента до 15 сентября.</p>
  						<button class="brown_btn" data-toggle="modal" data-target="#train_modal">Записаться на тренировку</button>
  					</div>
  				</div>
  			</div>

  			<div class="carousel-item">
  				<img src="<?php bloginfo('template_url'); ?>/img/header_slide-1.jpg" alt="BANE CrossFit" />

  				<div class="carousel-caption">
  					<div class="caption_inner">
  						<p class="header_1">B.A.N.E. CrossFit</p>
  						<p class="header_2">Скидка на вводный курс!</p>
  						<p class="header_5">Мы всегда рады новичкам в нашем зале.</p>
  						<p class="header_5">Вливайся в кроссфит-движение, <br>теперь всего лишь за 2000 рублей (8 тренировок).</p>
  						<p class="header_6">При покупке абонемента до 15 сентября.</p>
  						<button class="brown_btn" data-toggle="modal" data-target="#train_modal">Записаться на тренировку</button>
  					</div>
  				</div>

  			</div>
  		</div>

  		<!-- Modal -->
  		<div class="modal fade" id="train_modal" tabindex="-1" role="dialog" aria-labelledby="train_modal" aria-hidden="true">
  			<div class="modal-dialog" role="document">
  				<div class="modal-content">
  					<div class="modal-header">
  						<p class="modal-title header_3" id="train_modal_label">Заказать обратный звонок</p>
  						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  							<span><i class="fa fa-times" aria-hidden="true"></i></span>
  						</button>
  					</div>
  					<form>
  						<input type="text" name="name" placeholder="Имя" />
  						<input type="tel" class="phone" name="phone" placeholder="Телефон" />
  						<button type="button" class="white_btn">Заказать</button>
  					</form>
  				</div>
  			</div>
  		</div>

  	</div>
  </header>

  <main>