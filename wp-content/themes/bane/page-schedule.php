<?php
/*
Template Name: Schedule
*/
?>

<?php get_header(); ?>

<div class="schedule_section section before_section_bg texture_bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p class="header_3">Расписание</p>
				<p class="description">Прийти на пробную тренировку можно в любое время и на любой формат, по предварительной записи. OPEN GYM действует во всё время работы зала, но приоритет места и оборудования принадлежит группе. Нас очень расстраивает, когда вы опаздываете на тренировки - не нужно так:)</p>
				<table>
					<thead>
						<tr>
							<th class="time"></th>
							<th>Понедельник</th>
							<th>Вторник</th>
							<th>Среда</th>
							<th>Четверг</th>
							<th>Пятница</th>
							<th class="red">Суббота</th>
							<th class="red">Воскресенье</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="time">
								<span>07:00-08:00</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit_introductory">
								<span>Crossfit</span>
								<span>Вводный <br>курс</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit_introductory">
								<span>Crossfit</span>
								<span>Вводный курс</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit_introductory">
								<span>Crossfit</span>
								<span>Вводный курс</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
						</tr>
						<tr>
							<td class="time">
								<span>08:00-09:00</span>
							</td>
							<td class="athletics">
								<span>Тяжелая <br>атлетика</span>
							</td>
							<td class="athletics">
								<span>Тяжелая <br>атлетика</span>
							</td>
							<td class="athletics">
								<span>Тяжелая <br>атлетика</span>
							</td>
							<td class="athletics">
								<span>Тяжелая <br>атлетика</span>
							</td>
							<td class="athletics">
								<span>Тяжелая <br>атлетика</span>
							</td>
							<td class="athletics">
								<span>Тяжелая <br>атлетика</span>
							</td>
							<td class="athletics">
								<span>Тяжелая <br>атлетика</span>
							</td>
						</tr>
						<tr>
							<td class="time">
								<span>09:00-10:00</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
						</tr>
						<tr>
							<td class="time">
								<span>10:00-11:00</span>
							</td>
							<td class="open_gym">
								<span>Open Gym</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="open_gym">
								<span>Open Gym</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="open_gym">
								<span>Open Gym</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
						</tr>
						<tr>
							<td class="time">
								<span>11:00-12:00</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
						</tr>
						<tr>
							<td class="time">
								<span>12:00-13:00</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
						</tr>
						<tr>
							<td class="time">
								<span>13:00-14:00</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
						</tr>
						<tr>
							<td class="time">
								<span>14:00-15:00</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
						</tr>
						<tr>
							<td class="time">
								<span>15:00-16:00</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
						</tr>
						<tr>
							<td class="time">
								<span>16:00-17:00</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
						</tr>
						<tr>
							<td class="time">
								<span>17:00-18:00</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
						</tr>
						<tr>
							<td class="time">
								<span>18:00-19:00</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
						</tr>
						<tr>
							<td class="time">
								<span>19:00-20:00</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
						</tr>
						<tr>
							<td class="time">
								<span>20:00-21:00</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
						</tr>
						<tr>
							<td class="time">
								<span>21:00-22:00</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
							<td class="crossfit">
								<span>Crossfit</span>
							</td>
						</tr>
					</tbody>
				</table>

				<div class="schedule_description wrapper">
					<div class="schedule_item">
						<div class="open_gym schedule_btn">
							<span>Open Gym</span>
						</div>
						<div class="text">
							<p>Самостоятельные тренировки.</p>
						</div>
					</div>

					<div class="schedule_item">
						<div class="crossfit schedule_btn">
							<span>Crossfit</span>
						</div>
						<div class="text">
							<p>Основная тренировочная группа.</p>
							<p>Для тех, кто закончил вводный курс или занимался до этого.</p>
						</div>
					</div>

					<div class="schedule_item">
						<div class="athletics schedule_btn">
							<span>Тяжелая <br>атлетика</span>
						</div>
						<div class="text">
							<p>Тренировки, на которых тебя научат работать со штангой.</p>
							<p>А также упражнения на развитие силы и скорости.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>