/* Phone Form Script */
jQuery(function($){
	$(".phone_form").mask("+7 (999) 999-9999");
});
/* End of Phone Form Script */


/* Mobile Navigation Scripts */
$(".top_header .navbar-toggler").on("click", function(e) {
	$(this).toggleClass("active");
	e.preventDefault();
});

$("footer .navbar-toggler").on("click", function(e) {
	$(this).toggleClass("active");
	e.preventDefault();
});

$(document).on("click",function(){
	$(".navbar-collapse").collapse("hide");
});

$(function() {
	$("#main_navigation").on("hide.bs.collapse", function(e) {
		$(".top_header .navbar-toggler").removeClass("active");
	});
});

$(function() {
	$("#footer_navigation").on("hide.bs.collapse", function(e) {
		$("footer .navbar-toggler").removeClass("active");
	});
});
/* End of Mobile Navigation Scripts */