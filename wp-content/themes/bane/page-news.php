<?php
/*
Template Name: News
*/
?>

<?php get_header(); ?>

<div class="all_news_section section before_section_bg texture_bg">
	<div class="container">
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p class="header_3">Новости</p>

				<div class="wrapper">

					<?php
					$args = array(
						'post_type' => 'news'
					);
					$services_query = new WP_Query( $args );
					while ($services_query->have_posts()) : $services_query->the_post(); ?>

					<?php include "template-parts/news.php"; ?>

				<?php endwhile;
				wp_reset_query();
				wp_reset_postdata(); ?>

			</div>


			<?php query_posts( array('post_type'=> 'news', 'paged'=>get_query_var('paged') ) ); ?>
			<?php wp_pagenavi();?>
		</div>
	</div>
</div>
</div>

<?php get_footer(); ?>