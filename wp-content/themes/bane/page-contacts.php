<?php
/*
Template Name: Contacts
*/
?>

<?php get_header(); ?>

<div class="contacts_section section before_section_bg texture_bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p class="header_3">Наши контакты</p>
				<div class="description">
					<p>«BANE Crossfit» — сеть спортивных клубов, оснащенных современными тренажерами, идеально подходящими для новичков и профессионалов.</p>
				</div>
				<div class="hours">
					<i class="fa fa-clock-o" aria-hidden="true"></i>
					<p><span>Пн-пт:</span> c 7:00 до 23:00</p>
					<p><span>Сб-вс и праздничные дни:</span> c 9:00 до 21:00</p>
				</div>
				<p class="address">
					<i class="fa fa-map-o" aria-hidden="true"></i>
				г. Санкт-Петербург, ул. Марата, д. 23</p>
				<div class="phone_wrapper">
					<i class="fa fa-phone" aria-hidden="true"></i>
					<a href="tel:+7-812-812-12-12" class="phone bold">+7 (812) 812 -1212</a>
				</div>
				<div class="mails">
					<i class="fa fa-envelope-o" aria-hidden="true"></i>
					<p>Отдел продаж:</p>
					<a href="mailto:bane_sale@yandex.ru">bane_sale@yandex.ru</a>
					<p>Сервис-служба:</p>
					<a href="mailto:bane_service@yandex.ru">bane_service@yandex.ru</a>
					<p>Фитнес-департамент:</p>
					<a href="mailto:bane_fitness@yandex.ru">bane_fitness@yandex.ru</a>
				</div>
			</div>
		</div>
	</div>

	<div id="map" class="map">
		<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
		<script type="text/javascript">
			/* Yandex Api Map Script */
			ymaps.ready(function () {
				var myMap = new ymaps.Map('map', {
					center: [59.92789206420742,30.352860499999945],
					zoom: 17
				}, {
					searchControlProvider: 'yandex#search'
				}),

				myPlacemark = new ymaps.Placemark([59.92789206420742,30.352860499999945], {
					balloonContent: '<p class="baloon">Санкт-Петербург, ул. Марата, д. 23</p>'
				}, {

               // iconLayout: 'default#image',
               // iconImageHref: 'img/icons/map_baloon.png',
               preset: 'islands#redSportIcon',
               iconColor: '#b31c27',

               balloonCloseButton: true,
               balloonPanelMaxMapArea: 'Infinity'
             });

				myMap.behaviors.disable('scrollZoom'),

				myMap.geoObjects
				.add(myPlacemark);
			});
			/* End of Yandex Api Map Script */
		</script>
	</div>
</div>

<?php get_footer(); ?>