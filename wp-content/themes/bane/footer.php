</main>

<footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="navbar navbar-expand-lg">
					<button type="button" data-target="#footer_navigation" data-toggle="collapse" class="navbar-toggler">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<?php
					wp_nav_menu( array(
						'menu'              => 'footer_menu',
						'depth'             => 1,
						'container'         => 'div',
						'container_id'      => 'footer_navigation',
						'container_class'   => 'navbar-collapse collapse',
						'menu_class'        => 'footer_navigation nav navbar-nav',
						'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
						'walker'          => new WP_Bootstrap_Navwalker())
				);
				?>

			</div>
		</div>
	</div>
</div>
</footer>

<?php wp_footer(); ?>
</body>
</html>